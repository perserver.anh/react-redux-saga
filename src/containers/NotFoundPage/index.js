import { withStyles } from '@material-ui/core/styles';
import React from 'react';
import { Typography } from '@material-ui/core';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import styles from './styles';

function NotFoundPage(props) {
  const { classes } = props;
  return (
    <div className={classes.notfoundpage}>
      <div className={classes.content}>
        <Typography className={classes.title}>404</Typography>
        <Typography className={classes.subtitle}>Không Tìm Thấy!</Typography>
        <Typography variant="caption">
          Xin lỗi, nhưng trang này không tồn tại.
        </Typography>
        <Typography variant="caption">
          Trở lại trang <Link to="/login">đăng nhập</Link>
        </Typography>
      </div>
    </div>
  );
}

NotFoundPage.propTypes = {
  classes: PropTypes.string.isRequired
};

export default withStyles(styles)(NotFoundPage);
